![](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)  
![](https://img.shields.io/badge/license-AGPL3.0-informational?logo=gnu&color=important)


Ceci est un exemple de site web construit grâce à [Hugo](https://gohugo.io) et à [GitLab Pages](https://about.gitlab.com/features/pages/).

L'adresse du site web construit est la suivante : https://misha.gitpages.huma-num.fr/demo/hugo/ .

La documentation officielle d'Hugo se trouve [à cette adresse](https://gohugo.io/documentation/).
Vous pouvez en apprendre davantage sur les GitLab Pages [sur cette page](https://pages.gitlab.io) ; la documentation officielle se trouve [à cette adresse](https://docs.gitlab.com/ce/user/project/pages/).

---

### Créer votre propre site

Pour créer très simplement votre propre site web, suivez les étapes suivantes :

1. [Forkez ce dépôt](../../forks/new) en un nouveau dépôt vous appartenant.
2. **Ne lisez pas la suite de ce texte ici !** Allez la lire sur votre dépôt à vous, après avoir fait l'étape **1.**.
3. Modifiez la première ligne du fichier [`config.yaml`](config.yaml).
   Changez l'adresse web qui s'y trouve (originellement "https://misha.gitpages.huma-num.fr/demo/hugo/") et remplacez-là par celle indiquée sur [cette page](../../../pages).
4. C'est tout ! Votre site est en ligne à l'adresse indiquée sur [cette page](../../../pages).

---

### Aller plus loin

* Chaque fichier du répertoire `content/page` correspond à une page « statique » de votre site.
* Chaque fichier du répertoire `content/post` correspond à un article de blog de votre site.
* Le fichier `content/_index.md` correspond à la page d'accueil de votre site.
* Le [fichier YAML](https://fr.wikipedia.org/wiki/YAML) [`config.yaml`](config.yaml) contient les paramètres de configuration de votre site.
* Le fichier [`README.md`](README.md) correspond à ce que vous êtes en train de lire.  
  Comme tous les fichiers se terminant par `.md`, c'est un [fichier Markdown](https://docs.framasoft.org/fr/grav/markdown.html).

---

### Qui je suis ?

Moi, c'est [Régis Witz](https://orcid.org/0000-0002-8064-0977). Enchanté ! ʕᵔᴥᵔʔ
