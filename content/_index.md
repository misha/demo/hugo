## Bienvenue !

Ce site web est construit grâce à [Hugo](https://gohugo.io) et à [GitLab Pages](https://about.gitlab.com/features/pages/).
Il peut être construit très rapidement. Par vous !

Comment ?

* Le fichier `content/_index.md` correspond à la page d'accueil de votre site, c'est à dire ce que vous êtes en train de lire.  
  Comme tous les fichiers se terminant par `.md`, c'est un [fichier Markdown](https://docs.framasoft.org/fr/grav/markdown.html).
* Chaque fichier du répertoire `content/page` correspond à une page « statique » de votre site.
* Chaque fichier du répertoire `content/post` correspond à un article de blog de votre site.
* Le [fichier YAML](https://fr.wikipedia.org/wiki/YAML) `config.yaml` contient les paramètres de configuration de votre site.

Pour plus de renseignements, rendez-vous sur ce [projet GitLab](https://gitlab.huma-num.fr/misha/demo/hugo) !
