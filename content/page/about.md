---
title: Ateliers en SHS
subtitle: Échanger, se former, découvrir
comments: false
---

Ces Ateliers sont des rendez-vous mensuels pour que les chercheur·euse·s,
doctorant·e·s et étudiant·e·s, novices ou confirmé·e·s, puissent :

- échanger sur les pratiques, méthodes et expériences
- discuter des humanités numériques
- se former aux outils
- découvrir les acteurs locaux et nationaux

### En savoir plus

Les Ateliers sont organisés par la Plateforme « Humanités Numériques » (PHUN).
Rendez-vous sur [cette page](https://www.misha.fr/plateformes/phun) pour davantage d'informations.
