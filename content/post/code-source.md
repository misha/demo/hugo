---
title: Citer du code machine
subtitle: En utilisant Hugo ou Pygments
date: 2022-02-22
tags: ["exemple", "code"]
---

Vous pouvez aussi citer du code machine, par exemple du [Python](https://fr.wikipedia.org/wiki/Python_(langage)) ou du [R](https://fr.wikipedia.org/wiki/R_(langage)).

<!--more-->

Ci-dessous, un extrait de code en langage R, cité en le délimitant avec ` ``` `.

```r
mafonction <- function(arg1, arg2) {
  # ... ici un peu de code ...
  return(object)
}

sommedescarres <- function(x) {
  return(sum(x^2))  # Renvoie la somme des carrés des éléments de x
}
```

Ci-dessous, le même code, cité en utilisant le shortcode `highlight`.

{{< highlight r >}}
mafonction <- function(arg1, arg2) {
  # ... ici un peu de code ...
  return(object)
}

sommedescarres <- function(x) {
  return(sum(x^2))  # Renvoie la somme des carrés des éléments de x
}
{{</ highlight >}}


Ci-dessous, la même chose avec des numéros de ligne, et en langage Python :

{{< highlight python "linenos=inline">}}
  def mafonction(arg1, arg2):
    # ... ici un peu de code ...
    return o

  def sommedescarres(x):
    return sum(x^2)  # Renvoie la somme des carrés des éléments de x
{{</ highlight >}}
