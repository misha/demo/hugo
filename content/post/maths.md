---
title: Formules mathématiques
subtitle: En utilisant KaTeX
date: 2022-02-20
tags: ["exemple", "math"]
---

Des formules mathématiques complexes peuvent être affichées en utilisant le module KaTeX.

<!--more-->

Vous pouvez insérer une formule directement dans le texte, en utilisant la syntaxe `\\( ... \\)`, comme ceci : \\( E = mc^2 \\).
Vous pouvez afficher des formules plus volumineuses en les encadrant de `$$` ou de `\\[...\\]`, comme ceci :

Première méthode :
$$
\phi = \frac{(1+\sqrt{5})}{2} = 1.6180339887\cdots
$$

Seconde méthode : (même formule, syntaxe différente)
\\[
\phi = \frac{(1+\sqrt{5})}{2} = 1.6180339887\cdots
\\]

Pour en savoir plus, consultez [ce wiki](http://tiddlywiki.com/plugins/tiddlywiki/katex/), ou directement [le dépôt de KaTeX](https://github.com/Khan/KaTeX).


### Exemple 1

```
$$
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
$$
```
$$
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
$$


### Exemple 2

```
$$
\frac{1}{\Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{\frac25 \pi}} = 1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {1+\frac{e^{-6\pi}} {1+\frac{e^{-8\pi}} {1+\cdots} } } }
$$
```
$$
\frac{1}{\Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{\frac25 \pi}} = 1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {1+\frac{e^{-6\pi}} {1+\frac{e^{-8\pi}} {1+\cdots} } } }
$$


### Exemple 3

```
$$
1 +  \frac{q^2}{(1-q)}+\frac{q^6}{(1-q)(1-q^2)}+\cdots = \prod_{j=0}^{\infty}\frac{1}{(1-q^{5j+2})(1-q^{5j+3})}, \quad\quad \text{for }\lvert q\rvert<1.
$$
```
$$
1 +  \frac{q^2}{(1-q)}+\frac{q^6}{(1-q)(1-q^2)}+\cdots = \prod_{j=0}^{\infty}\frac{1}{(1-q^{5j+2})(1-q^{5j+3})}, \quad\quad \text{for }\lvert q\rvert<1.
$$


### Exemple 4

[Rappelez-vous]({{< ref "syntaxe-markdown.md" >}} "Article sur la syntaxe Markdown") que certains caractères ont une signification bien précise en Markdown.
En conséquence, il est nécessaire d'utiliser des alternatives.
Vous pouvez utilisr la syntaxe complète de KaTeX [sur cette page](https://khan.github.io/KaTeX/docs/supported.html).

Par exemple, le caractère `'` peut être remplacé par `^\prime`:

$$
G^\prime = G - u
$$

Le caractère `"` peut être remplacé par `^{\prime\prime}`:

$$
G^{\prime\prime} = G^\prime - v
$$
