---
title: Langage Markdown
subtitle: Introduction à la syntaxe
date: 2022-02-20
tags: ["exemple", "markdown", "images"]
---

Vous pouvez écrire directement du [Markdown](https://fr.wikipedia.org/wiki/Markdown), et il sera automatiquement converti en une page web.

<!--more-->

Au cas où vous en auriez besoin, [cette page-ci](https://www.leppf.com/site/spip.php?article154) décrit les principales choses à savoir concernant la syntaxe Markdown ; cela vous apprendra comment mettre du texte *en italiques* ou **en gras**, comment définir des titres de différents niveaux, comment construire des tables, et ainsi de suite.
Vous pouvez aussi consacrer quelques minutes pour apprendre pas à pas le Markdown [sur cette page-là](http://markdowntutorial.com/fr/).

**Ceci est du texte en gras**

## Voilà un titre secondaire

Que diriez vous d'une délicieuse crêpe ?

<!-- ceci est un commentaire, pour préciser que c'est trop bon les crêpes ! -->

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

Et pourquoi pas une table pour manger autour ?

| Colonne | Autre colonne | Et une autre |
| :------ |:------------- | :----------- |
| Cinq    | Six           | Quatre       |
| Dix     |    ʕᵔᴥᵔʔ      | Neuf         |
| Sept    | Huit          | ◖(｡◕‿◕｡)◗ ♪♫ |
| Deux | Trois | Deux mille vingt deux |

